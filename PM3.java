import java.util.*;

public class PM3 {
	
	// A global hash table that indexes each Person object under their name.
	public static HashMap<String, Person> allPeople = new HashMap<String, Person>();

	// Find person method
	public static Person findPerson(String name) {
		// Check if allPeople has key 
		if (allPeople.containsKey(name)) {
			// Return the Person object that name is mapped to
			return allPeople.get(name);
		}
		return null;
	}	// end findPerson

	// Driver
	public static void main(String[] args) {
		// Show table of options
		System.out.println("O <name>\t\t - Create a new person");
		System.out.println("P <name1> <name2>\t - Record name1 as parent of name2");
		System.out.println("M <name1> <name2>\t - Marry the two people");
		System.out.println("D <name>\t\t - Divorce person from their spouse");
		System.out.println("S <name>\t\t - Print the person's spouse");
		System.out.println("C <name>\t\t - Print the person's children");
		System.out.println("V <name>\t\t - Vaporize the person");
		System.out.println("T <name>\t\t - Print the person's tree of descendants");
		System.out.println("U <name>\t\t - Print the person's tree of ancestors");
		System.out.println("Z <name1> <name2>\t - Check if name2 is descendant of name1");
		System.out.println("X \t\t\t - eXit the program");
		System.out.println();
		System.out.println("Choose option: ");

		while (true) {
			// Prompt input
			Scanner sc = new Scanner(System.in);
			String input = sc.next();
			String name1 = sc.next();

			if (input.equals("O")) {
				// Use findPerson to check if person already exists
				if (findPerson(name1) != null) {
					System.out.println("Person already exists");
				}
				// If the person doesn't exist, create them
				if (findPerson(name1) == null) {
					// Create Person object
					Person newPerson = new Person(name1);
					// Add to allPeople (key = name1, value = newPerson)
					allPeople.put(name1, newPerson);
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end O

			if (input.equals("P")) {
				String name2 = sc.next();
				// findPerson for both names
				if (findPerson(name1) != null && findPerson(name2) != null) {
					Person found1 = findPerson(name1);
					Person found2 = findPerson(name2);
					// Test if descendants of each other
					if (found1.isDescendant(found2) || found2.isDescendant(found1)) {
						System.out.println("They're descendants");
					}
					else {
						// Set found1 as found2's parent
						found2.setParent(found1);
					}
				}
				else if (findPerson(name1) == null || findPerson(name2) == null) {
					System.out.println("Person doesn't exist.");
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end P

			if (input.equals("M")) {
				String name2 = sc.next();
				// findPerson for both names
				if (findPerson(name1) != null && findPerson(name2) != null) {
					Person found1 = findPerson(name1);
					Person found2 = findPerson(name2);
					// Test if descendants of each other
					if (found1.isDescendant(found2) || found2.isDescendant(found1)) {
						System.out.println("Can't marry descendants.");
					}
					else {
						// Call marry
						found2.marry(found1);
					}
				}
				else if (findPerson(name1) == null || findPerson(name2) == null) {
					System.out.println("Person doesn't exist.");
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end M

			if (input.equals("D")) {
				// findPerson in allPeople
				if (findPerson(name1) != null) {
					Person found1 = findPerson(name1);
					// Call divorce
					found1.divorce();
					System.out.println(found1.getName() + " is now divorced.");
				}
				else if (findPerson(name1) == null) {
					System.out.println("Person doesn't exist.");
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end D

			if (input.equals("S")) {
				// findPerson
				if (findPerson(name1) != null) {
					findPerson(name1).printSpouse();
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end S

			if (input.equals("C")) {
				// findPerson
				if (findPerson(name1) != null) {
					findPerson(name1).printChildren();
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end C

			if (input.equals("V")) {
				if (findPerson(name1) != null) {
					Person found1 = findPerson(name1);
					found1.vaporize(found1);
					System.out.println(found1.getName() + " is now vaporized.");
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end V
			
			if (input.equals("T")) {
				if (findPerson(name1) != null) {
					findPerson(name1).printDescendants();
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end T
			
			if (input.equals("U")) {
				if (findPerson(name1) != null) {
					findPerson(name1).printAncestors();
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end U
			
			if (input.equals("Z")) {
				String name2 = sc.next();
				// findPerson for both names
				if (findPerson(name1) != null && findPerson(name2) != null) {
					Person found1 = findPerson(name1);
					Person found2 = findPerson(name2);
					// Call isDescendant
					if (found2.isDescendant(found1)) {
						System.out.println("Yes");
					}
					else {
						System.out.println("No");
					}
				}
				else if (findPerson(name1) == null || findPerson(name2) == null) {
					System.out.println("Person doesn't exist.");
				}
				else {
					System.out.println("Error");
					return;
				}
			}	// end Z
			
			
			if (input.equals("X")) {
				// Close scanner and exit program
				sc.close();
				break;
			}
		}	
	}	// End main 
}
