import java.util.*;

public class Person {
	
	// Fields
	private String name;
	private Person parent1; 
	private Person parent2;
	private LinkedList<Person> children;
	private Person spouse;

	// Constructor
	public Person(String n) {
		name = n;
		children = new LinkedList<Person>();	    
	}

	// Getters
	public String getName() { 
		return name; 
	}
	public Person getParent1() { 
		return parent1;
	}
	public Person getParent2() { 
		return parent2;
	}
	public LinkedList<Person> getChildren() { 
		return children;
	}
	public int getNumChildren() { 
		return children.size();
	}
	public Person getSpouse() {
		return spouse;
	}

	public boolean setParent(Person q) {
		boolean succeed = false;
		if (q == null) {
			System.out.println("Parent is null");
		}
		else if (q == this) {
			System.out.println("A person cannot be their own parent");
		}
		else if (parent2 != null) {
			System.out.println(name + " already has two parents.");
		}
		else  {
			// Add child to the front of the children linked list of q.
			q.children.addFirst(this);
			if (parent1 == null) {
				parent1 = q;
			}
			else {
				parent2 = q;
			}
			succeed = true;
		}
		return succeed;
	} // end SetParent	

	public boolean marry(Person q) {
		if (q == null) {
			System.out.println("There is no one to marry.");
			//Exit method
			return false;
		}
		// Test if already married
		if (getSpouse() != null) {
			System.out.println(getName() + " is already married.");
			return false;
		}
		if (q.getSpouse() != null) {
			System.out.println(q.getName() + " is already married.");
			return false;
		}
		// If p is parent of q or vice versa, print error
		if (getParent1() == q || getParent2() == q) {
			System.out.println(q.getName() + " is a parent of " + getName());
			return false;
		}
		if (q.getParent1() == this || q.getParent2() == this) {
			System.out.println(getName() + " is a parent of " + q.getName());
			return false;
		}
		spouse = q;
		q.spouse = this;
		return true;
	} // end marry

	public boolean divorce() {
		Person q = getSpouse();
		// If unmarried
		if (q == null) {
			System.out.println(getName() + " is not married.");
			return false;
		}
		spouse = null;
		q.spouse = null;
		return true;
	} // end divorce

	// A method to delete a person q
	public boolean vaporize(Person q) {
		if (q == null) {
			System.out.println("Person doesn't exist.");
			return false;
		}
		else {
			// For q's parents, remove q as their child
			if (q.getParent1() != null) {
				for (int i = 0; i < q.getParent1().children.size(); i++) {
					if (q.getParent1().children.get(i) == q) {
						q.getParent1().children.remove(i);
					}
				}
			}
			if (q.getParent2() != null) {
				for (int i = 0; i < q.getParent2().children.size(); i++) {
					if (q.getParent2().children.get(i) == q) {
						q.getParent2().children.remove(i);
					}
				}
			}
			// Delete q as children's parent
			if (q.children != null) {
				for (int i = 0; i < q.children.size(); i++) {
					if (q.children.get(i).getParent1() == q) {
						q.children.get(i).parent1 = q.children.get(i).parent2;
						q.children.get(i).parent2 = null;
					}
					else if (q.children.get(i).getParent2() == q) {
						q.children.get(i).parent2 = null;
					}
				}
			}
			// Remove spousal relationships
			if (q.getSpouse() != null) {
				q.getSpouse().spouse = null;
				q.spouse = null;
			}
		}
		return true;	
	} // end vaporize
	
	public void printSpouse() {
		if (getSpouse() != null) {
			System.out.println(getName() + "'s spouse: " + spouse.getName());
		}
	}		// end printSpouse

	public void printChildren() {
		if (getChildren() != null) {
			String cnames = "";
			for (int i = 0; i < children.size(); i++) {
				cnames += children.get(i).getName() + " ";
			}
			System.out.println(getName() + "'s children: " + cnames);
		}
	}	// end printChildren

	// Check if descendant of q 
	public boolean isDescendant(Person q) {
		if (getParent1() == q) {
			return true;
		}			
		if (getParent2() == q) {
			return true;
		}
		return getParent1() != null && getParent1().isDescendant(q) || 
				getParent2() != null && getParent2().isDescendant(q);
	}	// end isDescendant

	public void printDescendants() {
		printDescendants1(0); 
	}
	public void printDescendants1(int indent) {
		if (getChildren() != null) {
			for (int i = 0; i < indent; i++) {
				System.out.print(" ");
			}
			System.out.println(getName());
			for (int j = 0; j < children.size(); j++) {
				children.get(j).printDescendants1(indent+3);
			}
		}
	}	// end printDescendants
	
	public void printAncestors() {
		printAncestors1(0);
	}
	public void printAncestors1(int indent) {
		for (int i = 0; i < indent; i++) {
				System.out.print(" ");
		}
		System.out.println(getName());
		if (getParent1() != null) {
			getParent1().printAncestors1(indent+3);
		}
		if (getParent2() != null) {
			getParent2().printAncestors1(indent+3);			
		}
	}	// end printAncestors
	
}
